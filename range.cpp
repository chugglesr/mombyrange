#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <sys/stat.h>
#include <cstdlib>
#include <algorithm>

#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TObject.h>
#include <TEventList.h>
#include <TBranch.h>
#include <TH1.h>
#include <TGraph.h>
#include <TGaxis.h>
#include <TMarker.h>
#include <TText.h>
#include <TMath.h>
#include <TSpectrum.h>
#include <TBox.h>
#include <TLatex.h>
#include <TString.h>
#include <TSystem.h>
#include "TApplication.h"
#include "TLegend.h"

#include "Lolirecon.hxx"
#include "B2_Dimension.cxx"
#include "B2EVENTSUMMARY.h"
#include "B2HitSummary.h"
#include "B2SimHitSummary.h"
#include "B2SimVertexSummary.h"
#include "B2SimParticleSummary.h"
#include "BeamInfoSummary.h"
#include "B2BasicReconSummary.h"
#include "B2ReconSummary.h"
#include "NeutInfoSummary.h"
using namespace std;

double EnergyLossIron(double dist_through, double m, double E){
//Assuming a minimum ionising muon, with 11.43MeV/cm then ~3cm will have 34.29MeV inserted 
	int q = 1; int Z = 26; int A = 56;
	double I = 286; //eV
	double ro = 6.95; // between 6.95-7.35 g/cm^3
	double k = 0.307075; // 0.307075 MeV cm^2/g
	double g, b2, EK, SP, EL, dist_cm;	
	dist_cm = dist_through/10; // dist through material in cm
	g = 1 + (E/m); // gamma factor  
	b2 = 1 - (1/(g*g)); // relativistic beta squared
	EK = (1.022*b2*g*g) / (1 + 2*g*(0.511/m) + pow(0.511/m,2) ); //Kinetic energy
	SP = ((ro*k*q*q*Z)/(A*b2))*(0.5*log( (1.022*b2*g*g*EK) / (I*I) ) - b2); //stopping power
	EL = SP*dist_cm; // energy loss

	return EL;
}
double EnergyLossScin(double dist_through, double m, double E){
//Assuming a minimum ionising muon, with 2.052MeV/cm then ~3cm will have 6.156MeV inserted 
	double I = 68.7; //eV
	int q = 1; int Z = 12; int A = 24;
	double ro = 1.060; // TEST WITH 1.030 g/cm^3
	double k = 0.307075; // 0.307075 MeV cm^2/g
	double g, b2, EK, SP, EL, dist_cm;
	dist_cm = dist_through/10; // dist through material in cm
	g = 1 + (E/m); // gamma factor  
	b2 = 1 - (1/(g*g)); // relativistic beta squared
	EK = (1.022*b2*g*g) / (1 + 2*g*(0.511/m) + pow(0.511/m,2) ); //Kinetic energy
	SP = ((ro*k*q*q*Z)/(A*b2))*(0.5*log( (1.022*b2*g*g*EK) / (I*I) ) - b2); //stopping power
	EL = SP*dist_cm; // energy loss

	return EL;
}

// Returns true if x is in range [low..high], else false
bool inRange(double low, double high, double x){return (low <= x && x <= high);} 

//Globally defined detector geom
int detector_z[18] = {0, 250, 427, 537, 647, 889, 
			 1067, 1382, 1561, 1868, 2188, 2367,
                         2618, 2795, 3243, 3690, 4011, 4111};
int iron_z[33] = {70, 142, 210, 497, 607, 717,
                     959, 1029, 1137, 1210, 1452, 1519,
                     1631, 1700, 1938, 2008, 2078, 2148,
                     2437, 2506, 2578, 2865, 2937, 3004,
                     3074, 3313, 3383, 3454, 3521, 3760,
                     		       3833, 3902, 3971};
int gap_z[2] = {106, 176}; //Assumed  0 energy loss in these reigons
float ironwidth = 3.0; //cm
float scinwidth = 3.1; //cm -- will need to change due to HVVH structure of scintillator planes
float detEloss = 2.052; //MeV/cm
float ironEloss = 11.43; //MeV/cm

int detI(int startz, int endz){
 int deti=0;
 for (int i=0; i<18; i++){
  if (inRange(startz, endz, detector_z[i]) == true) deti++;
  else continue;
 }
 return deti;
}

int whichfirstscin(float startz, float endz){
  if (startz != 0){
    for (int i=0; i<18; i++){
       if (startz > detector_z[i] && startz < detector_z[i+1] && endz > detector_z[i+1]){
           return detector_z[i+1];
        }
	else if (startz > detector_z[i] && startz < detector_z[i+1] && endz < detector_z[i+1]){startz=-1; return startz;}
        else {continue;}
     }
   }
   else if (startz == 0){
        return startz;
     }
}

int whichlastscin(float startz, float endz){
   if (endz < 4111 && endz > 0){
     for (int i=17; i --> 0; ){
         if (endz < detector_z[i] && endz > detector_z[i-1] && startz < detector_z[i-1]){
              return detector_z[i-1];
         }
         else {continue;}
      }
    }
   else if (endz >= 4111){
        return 4111;
     }
}

float detE(int deti){
 float detE = deti * scinwidth * detEloss;
 return detE;
}

int ironI(int startz, int endz){
 int ironi = 0;
 for (int i=0; i<33; i++){
  if (inRange(startz, endz, iron_z[i]) == true) ironi++;
  else continue;
 }
 return ironi;
}

float ironE(int ironi){ 
 float ironE = (ironi * ironwidth) * ironEloss;
 return ironE;
}

long fcTime;

/*----------------------------------------------------------------------------------------------------
gROOT->SetStyle("Modern");		// set style to Modern - looks nice 
gStyle->SetOptStat(111111);		// draw statistics on plots, (0) for no output
gStyle->SetOptFit(1111);		// draw fit results on plot, (0) for no output
//gStyle->SetPalette(800);		// set color map
//gStyle->SetOptTitle(0);		// suppress title box

----------------------------------------------------------------------------------------------------*/

int main(int argc, char *argv[]){
  char buff1[200];//,buff2[200];
  char Output[300];
  TROOT root("GUI","GUI");
  TApplication theApp("App",0,0);
  Int_t c=-1;
  Int_t Nini = 0;
  
  while ((c = getopt(argc, argv, "f:o:i:")) != -1) {
    switch(c){
    case 'o':
      sprintf(Output,"%s",optarg);
      break;
    case 'f':
      sprintf(buff1,"%s",optarg);
      break;
    case 'i':
      Nini=atoi(optarg);
      break;
    }
  }


int nBins = 100; int minValue = 0; int maxValue = 1300;
TH1F *muonenergy = new TH1F("Muon_Energy", "E(#mu) in Baby MIND", nBins, minValue, maxValue);
TH1F *pi0        = new TH1F("0#pi", "E(#mu) in Baby MIND", nBins, minValue, maxValue);
TH1F *pi1        = new TH1F("1#pi", "E(#mu) in Baby MIND", nBins, minValue, maxValue);
TH1F *DIS        = new TH1F("multi#pi", "E(#mu) in Baby MIND", nBins, minValue, maxValue);
TH1F *CCQE       = new TH1F("CCQE", "E(#mu) in Baby MIND", nBins, minValue, maxValue);
TH1F *length     = new TH1F("Length", "Track length", nBins, 0, 4200);
TH2F *TL_EL = new TH2F("TL_EL","(Track length, E_{Recon})",nBins,0,4400,nBins,0,1600);
TH1F *EovaLength    = new TH1F("EovaLength", "E_{Recon}/Track length", nBins, 0, 1.5);    
TH1F *RatioHist     = new TH1F("Ratio", "E_{Recon}/Track length", nBins, 0, 1);
TH1F *LastScinHit    = new TH1F("LastScinHit", "Last Scintillator Hit", nBins, 0, 4200);
TH1F *reconstructed    = new TH1F("reconstructed", "L_{Recon}", nBins, 0, 4200);
TH2F *LastScinvsE = new TH2F("lastscinvsE","(Last scin hit,E_{Recon})",nBins,0,4200,nBins,0,1500);
TH2F *reconstructedVsE = new TH2F("reconstructedvsE","(L_{Recon},E_{Recon}) (MeV/mm)",nBins,0,4200,nBins,0,1500);
TH1F *residualE    = new TH1F("residualE", "Residual Energy", nBins, -100, 100);
TH1F *residualL    = new TH1F("residualL", "Residual Length", nBins, -400, 400);

        Int_t Scyc =  4;
        Int_t Ncyc = 12;
        Int_t Nmod = 6;
 	bool candy = true;
 	FileStat_t fs;

	B2EventSummary* evt = new B2EventSummary();
  	TFile*             rfile = new TFile("inputfile.root","read");
	TTree*              tree = (TTree*)rfile -> Get("tree;1");
	TBranch*           EvtBr = tree->GetBranch("fDefaultReco.");
  	EvtBr                    ->SetAddress(&evt);
  	tree                     ->SetBranchAddress("fDefaultReco.", &evt);
  	int                 nevt = (int)tree -> GetEntries();
  	cout << "Total # of events = " << nevt <<endl;
	
  	TFile*             wfile = new TFile(Output, "recreate");
  	TTree*             wtree = new TTree("tree","tree");
  	wtree                   -> SetMaxTreeSize(5000000000);
  	B2EventSummary* wsummary = new B2EventSummary(); 
  	wtree                   -> Branch("fDefaultReco.","B2EventSummary",&wsummary,64000,99);
  	B2HitSummary* inghitsum;
  	B2SimVertexSummary* simver;
  	B2ReconSummary* recon = new B2ReconSummary();
  	Hit                        hit;



  	for(int ievt=0; ievt<nevt; ievt++){//event loop
    	if(ievt%100==0){cout << "Analyse event #" << ievt << endl;}// candy = true;}
    	//if(ievt%100!=0){candy = false;}
    	wsummary -> Clear();
    	evt -> Clear();
    	tree -> GetEntry(ievt);

    	Scyc=4;
    	Ncyc=5;

    	for(int cyc=Scyc; cyc<Ncyc; cyc++){
      	for(int mod=0; mod<Nmod; mod++){

        int ninghit = evt-> NB2ModHits(mod,cyc);
 	hitcls.clear();
 	alltrack.clear();

 	for(int i=0; i<ninghit; i++){
	  inghitsum = (B2HitSummary*) (evt->GetB2ModHit(i,mod,cyc));	
	  
	  if(inghitsum -> pe < 2.5) continue;
	  inghitsum -> addbasicrecon = false;

	  hit.mod = mod;
	  hit.id = i;
	  hit.pe = inghitsum -> pe;
	  hit.lope = inghitsum -> lope;
	  hit.time  = (Long_t)inghitsum -> time;
	  hit.view  = inghitsum -> view;
	  hit.pln   = inghitsum -> pln;
	  hit.ch    = inghitsum -> ch;
	  //inghitsum -> z = 
	  hitcls.push_back(hit);
	}
	if(hitcls.size() < 2) continue;
	//int nactnum;
	int alltrack_size_bak=0;
	int Nconnect=0;
	simver = (B2SimVertexSummary*) (evt -> GetSimVertex(0));
	if(mod==1||mod==2){
	  fTracking_loli(mod);
	  alltrack_size_bak=(int)alltrack.size();
	  while(fConnectTracks(20, 2*25)){
	    Nconnect++;
	    if(Nconnect>alltrack_size_bak) break;
	  }
	}
	else{
	  fTracking(mod);
	}


	for(int i=0;i<(int)alltrack.size();i++){
	  recon -> Clear();
	  recon -> eventid = ievt;
	  recon -> startz          = alltrack[i].iz;  
	  recon -> endz            = alltrack[i].fz; 

	  int detnum = detI(alltrack[i].iz,alltrack[i].fz);
	  int ironnum = ironI(alltrack[i].iz,alltrack[i].fz);
	  float Eloss = fabs(ironE(ironnum)) + fabs(detE(detnum));
	  float tracklength = alltrack[i].fz - alltrack[i].iz;
	  float ratio = Eloss/tracklength;

	  if (ratio <= 1){
	  if ((simver->inttype) == 1){
	     CCQE->Fill(Eloss,1);    }
	  else if ((simver->inttype) == 16){
	     pi0->Fill(Eloss,1);     }
	  else if ((simver->inttype) == 11 || (simver->inttype) == 12 || (simver->inttype) == 13){
	     pi1->Fill(Eloss,1);     }
	  else if ((simver->inttype) == 26){
	     DIS->Fill(Eloss,1);     }
	 //Collate NC: 31,32,33,34,36,37,38,41,42,43,44,45, 
	 /*
 	 31 NC1pi0, 32 NC1pi0, 33NC1pi-, 34 NC1pi+, 36 NC0pi, 37 NC1gamma, 
	 38 NC1gamma, 41 NCmultipi, 42 NC1eta, 43 NC1eta, 44 NC1K0,
	 45 NC1K, 46 NCDIS, 51 NCE, 52 NCE 
	 */
	  muonenergy->Fill(Eloss,1);	 
	  length->Fill(tracklength,1); 
          TL_EL->Fill(tracklength,Eloss);
	  EovaLength->Fill(Eloss/tracklength,1);
	  RatioHist->Fill(ratio,1);
          int firstscin = whichfirstscin(alltrack[i].iz,alltrack[i].fz);
          int lastscin = whichlastscin(alltrack[i].iz,alltrack[i].fz);
	  
	  if (lastscin > 1 && firstscin != lastscin){
	    cout << "startz: "<< alltrack[i].iz << "\t endz: " << alltrack[i].fz << "\t first: " << firstscin << "\t last: " << lastscin << endl;
	    LastScinHit->Fill(lastscin,1);
    	    int reconlength = lastscin - firstscin; cout << "L_recon: " << reconlength << endl; 

	    LastScinvsE->Fill(reconlength,Eloss);
	    reconstructed->Fill(reconlength,1);

	    int recodetnum = detI(firstscin,lastscin);
            int recoironnum = ironI(firstscin,lastscin);
            float recoEloss = fabs(ironE(recoironnum)) + fabs(detE(recodetnum));

	    reconstructedVsE->Fill(reconlength,recoEloss);
	    residualL->Fill(reconlength-tracklength,1);
	    residualE->Fill(recoEloss-Eloss,1);
	  }
	  }
	  
	  for(int NHIT=0;NHIT<(int)alltrack[i].hitid.size();NHIT++){
	    inghitsum   = (B2HitSummary*) (evt -> GetB2ModHit(alltrack[i].hitid[NHIT], mod, cyc) );
	    inghitsum -> isohit   = alltrack[i].isohit[NHIT];
	    inghitsum -> gocosmic = true;
	    recon -> AddB2Hit(inghitsum);
	  }
	  recon -> clstime      = fcTime;
	  recon -> hitmod       = mod;
	  recon -> hitcyc       = cyc;
	  //evt   -> AddB2Recon( recon );
	  evt   -> AddB2ModRecon(recon, mod, cyc, alltrack[i].view);
	}
      }
    }
  //wsummary = evt;
  //wtree -> Fill();
  }//event loop
  rfile->Close(); 


  TFile* wrfile = new TFile("output.root", "RECREATE"); 

  CCQE->Write(); pi0->Write(); pi1->Write(); DIS->Write();
  muonenergy->GetXaxis()->SetTitle("E_{Recon} (MeV)"); muonenergy->Write(); 
  length->GetXaxis()->SetTitle("L_{Track} (mm)"); length->Write();
  EovaLength->GetXaxis()->SetTitle("#frac{E}{L} (MeV mm^{-1})"); EovaLength->Write(); 
  RatioHist->GetXaxis()->SetTitle("#frac{E}{L} (MeV mm^{-1})"); RatioHist->Write();

  TL_EL->GetYaxis()->SetTitle("E_{Recon} (MeV)"); TL_EL->GetXaxis()->SetTitle("L_{Track} (mm)");
  TL_EL->Write();

  LastScinHit->GetXaxis()->SetTitle("Last scin hit position (mm)"); LastScinHit->Write();

  LastScinvsE->GetXaxis()->SetTitle("Last scin hit position (mm)"); LastScinvsE->GetYaxis()->SetTitle("E_{Recon} (Mev)"); 
  LastScinvsE->Write();

  reconstructed->GetXaxis()->SetTitle("L_{Recon} (mm)");
  reconstructed->Write(); 

  reconstructedVsE->GetXaxis()->SetTitle("L_{Recon} (mm)"); reconstructedVsE->GetYaxis()->SetTitle("E_{Recon} (MeV)"); 
  reconstructedVsE->Write();

  residualL->GetXaxis()->SetTitle("Residual length (mm)"); residualL->Write();
  residualE->GetXaxis()->SetTitle("Residual Energy (MeV)"); residualE->Write();
  wrfile->Close();

}
