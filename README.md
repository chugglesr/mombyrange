MomByRange

This script takes an input .root file that is the output of the Lolirecon executable in the wagasci_analysis package (file inputfile.root has been provided)
To run:
Correct instances of "---LocationOf---" with corresponding paths to directories
run make
$ ./range

Initial MC generated from wagasci-babymind Monte Carlo B2MC executable, with input parameters:
-i
-f 1
-d 2
-v 3
-w 3
-m

NEUT file used: 13a_nd7_posi250_numu_h2o_1.nt
~                                             
