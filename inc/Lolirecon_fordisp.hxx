#ifndef __LOLIRECON_HXX__
#define __LOLIRECON_HXX__

//#define PMEDGE

// ROOT includes
#include "TApplication.h"
#include "TFile.h"
#include "TH1F.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TH2F.h"
#include "TCanvas.h"
#include <TStyle.h>
#include "TString.h"
#include "TSystem.h"
#include "TSpectrum.h"
#include "TTree.h"
#include "TSpline.h"
//C++ libraly includes
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <deque>
#include <vector>
#include <sys/stat.h>
#include <unistd.h> // using getopt      
using namespace std;
// B2 includes

//#include "Lolirecon.hxx"
#include "B2_Dimension.cxx"
#include "B2SimVertexSummary.h"
#include "B2SimParticleSummary.h"


const int Cview = 2;
const int Cpln = 200;
const int Cch = 200;
const int Ccls = 200;
const int Ccell = 5000;
const int Chit = 5000;
const int Ctra = 5000;
//const int Cdist = 4;
const int Cdist = 6;
const int Cmod = 10;
const int Cdir = 3;
const int MAX_CELLVALUE=200;


//__________________________________________________________


class Hit{
public:
  Int_t       id;   //b/w Hit class and B2HitSummary
  Int_t      mod;
  Int_t      pln;
  Int_t     view;
  Int_t       ch;
  Int_t     used;
  //Int_t      adc;
  Float_t     pe;   //p.e.
  Float_t     pe_cross;   //p.e.
  Float_t   lope;   //p.e.
  Long_t     tdc;  
  //Long_t    nsec; 
  Long_t    time;   //nsec
  //Long_t    tnearhit;   //nsec
  double   posxy;
  double    posz;
  //Int_t      pdg;

  void clear(){
    id   =  -1;
    mod  =  -1;
    pln  =  -1;
    view =  -1;
    ch   =  -1;
    //adc  =  -1;
    pe   =  -1.e-5;
    lope =  -1.e-5;
    tdc  =  -1;
    //nsec =  -1;
    time =  -1;
    //tnearhit = -1;
    posxy=  -1.e-5;
    posz =  -1.e-5;
    //pdg  =  -1;
  }
};
vector<Hit> allhit;
vector<Hit> allhit_for_disp;
vector<Hit> hitcls;
vector<Hit> hitcls_for_joint;
B2SimVertexSummary* simver_for_disp;
B2SimParticleSummary* simpar_for_disp;

class Track{
public:
  Int_t     mod;
  Int_t     view;
  Int_t     ipln;
  Int_t     fpln;
  Float_t    ixy;
  Float_t    fxy;
  Float_t     iz;
  Float_t     fz;
  Float_t  slope;
  Float_t intcpt;
  Float_t    ang;
  Float_t  clstime;
  Bool_t    veto;
  Bool_t    edge;
  Bool_t    stop;
  Float_t vetodist;
  //Float_t totpe;
  //Int_t isohit;
  vector<Int_t> hitid;
  vector<Bool_t> isohit;
  //Int_t pdg[4];

  void clear(){
    mod    =  -1;
    view   =  -1;
    ipln   =  -1;
    fpln   =  -1;
    ixy    =  -1.e-5;
    fxy    =  -1.e-5;
    iz     =  -1.e-5;
    fz     =  -1.e-5;
    slope  =  -1.e-5;
    intcpt =  -1.e-5;
    ang    =  -1.e-5;
    clstime=  -1.e-5;
    veto   = false;
    edge   = false;
    stop   = false;
    vetodist   =  -1.e-5;
    hitid.clear();
    isohit.clear();
    //totpe   =  -1.e-5;
    //isohit   =  -1;
    //memset(pdg,-1,sizeof(pdg));
  }
};

vector<Track> alltrack;


int chmax(int mod, int view, int pln, int axis=0){
  int maxch=0;
  if(mod==6)maxch=24;
  else if(mod==1||mod==2){
    B2_Dimension *fdim = new B2_Dimension();
    maxch = fdim -> get_chmax_loli( mod, view, pln, axis); //mod view pln axis
    delete fdim;
  }
  else if(mod==0){
    //if(pln==0)maxch=24;
    //else maxch=32;
    maxch=32;
  }
  else if(mod==3||mod==4){
    maxch=8;
  }
  else if(mod==5){
    if(view==0) maxch=95;
    else maxch=16;
  }
  return maxch;
}

int plnmax(int mod){
  int maxpln=0;
  if(mod==6)maxpln=11;
  else if(mod==1 || mod==2){
    maxpln = 60;
  }
  else if(mod==0) maxpln=18;
  else if(mod==3 || mod==4){
    maxpln = 10;
  }
  else if(mod==5) maxpln=18;
  return maxpln;
}

int plnmax(int mod, int view, int pln, int axis=0){
  int maxpln=0;
  if(mod==6)maxpln=11;
  else if(mod==1 || mod==2){
        B2_Dimension *fdim = new B2_Dimension();
        maxpln = fdim -> get_plnmax_loli( mod, view, pln, axis); //mod view pln axis
	delete fdim;
  }
  else if(mod==0) maxpln=18;
  else if(mod==3 || mod==4){
    maxpln = 10;
  }
  else if(mod==5) maxpln=18;
  return maxpln;
}


float zposi(int mod,int view,int pln){
  double detector_module_z[18] = {0, 25, 42.7, 53.7, 64.7, 88.9, 106.7,
                                  138.2, 156.1, 186.8, 218.8, 236.7,
                                  261.8, 279.5, 324.3, 369, 401.1, 411.1};
  double posiz;
  if(mod==0){
    if(view==0){
      if(pln==0)posiz=5;
      else posiz=46*pln+9;
    }
    else{
      if(pln==0)posiz=28;
      else posiz=46*pln+32;
    }
  }
  else if(mod==1||mod==2){
    return 0;
  }
  else if(mod==3||mod==4){
    posiz=43*pln;
  }
  else if(mod==5){
    if(pln<18)posiz=detector_module_z[pln]*10;
    else{
      cout<<"BABYMIND PLN error"<<endl;
      exit(1);
    }
  }
  else if(mod==6){
    if(view==0){
      posiz=5+105*pln+1074.5;
    }
    else{
      posiz=5+105*pln+1074.5+10;
    }
  }
  return posiz;
}


float zposi(int mod,int view,int pln, int ch, int axis=0){
  double detector_module_z[18] = {0, 25, 42.7, 53.7, 64.7, 88.9, 106.7,
                                  138.2, 156.1, 186.8, 218.8, 236.7,
                                  261.8, 279.5, 324.3, 369, 401.1, 411.1};
  double posiz, posixy;
  if(mod==0){
    if(view==0){
      if(pln==0)posiz=5;
      else posiz=46*pln+9;
    }
    else{
      if(pln==0)posiz=28;
      else posiz=46*pln+32;
    }
  }
  else if(mod==1||mod==2){
        B2_Dimension *fdim = new B2_Dimension();
        fdim -> get_posi_lolirecon( mod, view, pln, ch, axis, &posixy, &posiz); //mod view pln ch axis in recon
	//posiz = 10. * posiz; //cm -> mm
	if(axis==0)posiz = posiz*10  + 409.5; //shift
	if(axis==1)posiz = posixy*10 + 600.; //shift
	delete fdim;
  }
  else if(mod==3||mod==4){
    posiz=43*pln;
  }
  else if(mod==5){
    if(pln<18)posiz=detector_module_z[pln]*10;
    else{
      cout<<"BABYMIND PLN error"<<endl;
      exit(1);
    }
  }
  else if(mod==6){
    if(view==0){
      //posiz=5+105*pln+1074.5;
      posiz=5+105*pln+1074.5+10;
    }
    else{
      //posiz=5+105*pln+1074.5+10;
      posiz=5+105*pln+1074.5;
    }
  }  
  return posiz;
}

float xyposi(int mod, int view, int pln,int ch, int axis=0){
  double posixy, posiz;
  
  if(mod==0){
    if(pln==0){
      posixy=ch*50+25;
    }
    else{
      if(ch<8)posixy=ch*50+25;
      else if(ch<24)posixy=412.5+(ch-8)*25;
      else posixy=(ch-8)*50+25;
    }
  }
  else if(mod==1||mod==2){
      B2_Dimension *fdim = new B2_Dimension();
      fdim -> get_posi_lolirecon( mod, view, pln, ch, axis, &posixy, &posiz); //mod view pln ch axis in recon
      //posixy = 10. * posixy; //cm -> mm
      if(axis==0)posixy = posixy*10 + 600.; //shift
      if(axis==1)posixy = posiz*10  + 409.5; //shift
      delete fdim;
  }
  else if(mod==3||mod==4){
    posixy=ch*200+100;
  }
  else if(mod==5){
    if(view==0){
      posixy=ch*20.4+10.2;
    }
    else{
      posixy=ch*178+178/2;
    }
  }
  else if(mod==6){
    posixy=ch*50+25;
  }
  return posixy;
}

float vposiz(int mod,int ch){
  float X;
  //if(mod!=16)
  //if(mod!=15)
  if(mod!=0)
    X=5+9.5+50*ch+1075;
  else
    X=5+9.5+50*ch;
  return X;
}


int vposixy(int mod,int pln){
  int Y=0;
  /*
  if(mod==0){
    if(pln==11)Y=(int)-105.75;  //right
    else if(pln==12)Y=1309;//left
    else if(pln==13)Y=-59; //botom
    else Y=1284;           //top
  }
  else if(mod<7){
    if(pln==11)Y=-191;  //right
    else if(pln==12)Y=1309;//left
    else if(pln==13)Y=-59; //botom
    else Y=1284;           //top
  }
  else if(mod==7){
    if(pln==11)Y=(int)-105.75;  //right
    else if(pln==12)Y=1309;//left
    else if(pln==13)Y=-59; //botom
    else Y=1284;           //top
  }
  else if(mod<14){
    if(pln==11)Y=(int)-105.75;  //right
    else if(pln==12)Y=1309;//left
    else if(pln==13)Y=-216; //botom
    else Y=1284;           //top
  }
  */

  if(mod==6){
    if(pln==11)Y=(int)-105.75;  //right
    else if(pln==12)Y=1309;//left
    else if(pln==13)Y=-59; //botom
    else Y=1284;           //top
  }
  else{//proton module
    if(pln==18||pln==21)Y=-55;
    else Y=1255;
  }
  return Y;
}

float sciwidth(int mod, int view, int pln, int ch, int axis, int flag=0){
  float sciw;
  if(mod==1||mod==2){
        B2_Dimension *fdim = new B2_Dimension();
	sciw = fdim -> get_sciwidth(mod,view,pln,ch,axis);
	sciw = sciw*10.;
	delete fdim;
	//added by koga 2016/7/11
	if(flag==1){sciw=0.5;}
  }
  else if(mod==0){
    if(pln!=0&&ch>=8&&ch<24) sciw=12.5;
    else sciw=25.;
  }
  else if(mod==6){
    sciw=25.;
  }
  else if(mod==3||mod==4){
    sciw=100.;
  }
  else if(mod==5){
    if(view==0) sciw=15.5;
    else sciw=105.;
  }
  return sciw;
};


float scithick(int mod, int view, int pln, float xy, int axis, int flag=0){
  float scith;
  if(mod==1||mod==2){
        B2_Dimension *fdim = new B2_Dimension();
	scith = fdim -> get_scithick(mod,view,pln,0,axis); //mod view pln ch axis
	scith = scith*10.;
	delete fdim;
	//added by koga 2016/7/11
	if(flag==1){scith=0.5;}
  }
  else if(mod==0){
    if(pln!=0&&xy>400&&xy<800) scith=6.5;
    else scith=5;
  }
  else if(mod==6){
    scith=5;
  }
  else if(mod==3||mod==4){
    scith=3.5;
  }
  else if(mod==5){
    scith=3.75;
  }
  return scith;
};


float Yerr(int mod,int view, int pln,int ch, int axis, int edge){
  float yerr;
  if(edge==0)yerr=xyposi(mod,view,pln,ch,axis)-sciwidth(mod,view,pln,ch,axis);
  else       yerr=xyposi(mod,view,pln,ch,axis)+sciwidth(mod,view,pln,ch,axis);
  //if(edge==0)yerr=(xyposi(mod,view,pln,ch,axis)-sciwidth(mod,view,pln,ch,axis))/sqrt(12);
  //else       yerr=(xyposi(mod,view,pln,ch,axis)+sciwidth(mod,view,pln,ch,axis))/sqrt(12);
  //if(edge==0)yerr=xyposi(mod,view,pln,ch,axis)-sciwidth(mod,view,pln,ch,axis,1);
  //else       yerr=xyposi(mod,view,pln,ch,axis)+sciwidth(mod,view,pln,ch,axis,1);
  return yerr;
};






bool actpln[Cview][Cpln];
int nactpln;
int fNactpln(int mod){
  nactpln=0;

  if(plnmax(mod, 0, 0, 0)==0){cout<<"ERROR"<<endl;exit(1);}
  memset(actpln,false,sizeof(actpln));
  for(int i=0; i<(int)hitcls.size(); i++){
    //cout<<hitcls[i].pln<<" "<<hitcls[i].ch<<endl;
    if(hitcls[i].view!=1&&hitcls[i].view!=0){cout<<"Error :"<<hitcls[i].view<<endl;exit(1);}
    if(hitcls[i].pln<plnmax(mod, 0, 0, 0))actpln[hitcls[i].view][hitcls[i].pln]=true;
  }
  for(int i=0; i<plnmax(mod, 0, 0, 0); i++){
    /*
    if(mod!=16){
      if(actpln[0][i]&&actpln[1][i])nactpln++;
      else{
	actpln[0][i]=false;
	actpln[1][i]=false;
      }
    }
    else{
      if(actpln[0][i]&&actpln[1][i])nactpln++;
      else{
	actpln[0][i]=false;
	actpln[1][i]=false;
      }
    }
    */
    if(mod==3||mod==4){
      if(actpln[1][i])nactpln++;
      else{
	actpln[1][i]=false;
      }   
    }
    else{
      if(actpln[0][i]&&actpln[1][i])nactpln++;
      else{
	actpln[0][i]=false;
	actpln[1][i]=false;
      }
    }
  }
  return nactpln;
};



float fLayerpe(int mod){
  float layerpe=0;
  for(int i=0; i<(int)hitcls.size(); i++){
    if(hitcls[i].pln<plnmax(mod, 0, 0, 0)){
      if(actpln[hitcls[i].view][hitcls[i].pln])layerpe+=hitcls[i].pe;
    }
  }
  if(nactpln==0)layerpe=0;
  else layerpe=layerpe/nactpln;
  return layerpe;
};


bool withtime(const Hit& left, const Hit& right){
  return left.time < right.time;
};

void fSortTime(vector<Hit> &a){
  std::stable_sort(a.begin(), a.end(), withtime);
};

//Int_t cTdcRsr = 50; //nsec
Int_t cTdcRsr = 50; //nsec
float cPeCut  = 18;

bool fFindTimeClster(vector<Hit> &hit, vector<Hit> &hitclster, 
		     Long_t &ctime){
  //fSortTime( hit );
  Int_t nhit = hit.size();
  //int maxhit=5;
  //if(hit[0].mod==MOD_WAGASCI){
  //if(hit[0].mod==1 || hit[0].mod==2){
  //maxhit=1;
  //};

  //if(nhit <= maxhit)return false;

  
  vector<Hit>::iterator it;
  hitcls.clear(); //Reset hit clster
  for(it = hit.begin() ; it != hit.end(); it++){
    hitclster.push_back(*it);
    it = hit.erase(it);
    it--;
  }
  
  return true;

}

/*
bool fGetTNearHit(vector<Hit> &hit){
  int nhit = hit.size();
  if( nhit == 0 ){
    return true;
  }
  if( nhit == 1 ){
    hit[0].tnearhit = 0;
    return true;
  }

  for(int ihit=0; ihit<nhit; ihit++){
    if( ihit == nhit-1 )
      hit[ihit].tnearhit = abs( hit[ ihit ].time - hit[ ihit-1 ].time );
    if( ihit == 0 )
      hit[ihit].tnearhit = abs( hit[ ihit + 1 ].time - hit[ ihit ].time );

    else{
      long bnearhit = abs(hit[ihit+1].time - hit[ihit].  time);
      long anearhit = abs(hit[ihit]  .time - hit[ihit-1].time);
      hit[ihit].tnearhit = min( bnearhit, anearhit );
    }
  }
}

bool fMergeClster(vector<Hit> &hit1, vector<Hit> &hit2, 
		  vector<Hit> &mergehit ){


      //###### clstering ######
      //#######################
      vector<Hit>::iterator it;
      for(it = hit1.begin() ; it != hit1.end(); it++){
	mergehit.push_back(*it);
	it = hit1.erase(it);
	it--;
      }

      for(it = hit2.begin() ; it != hit2.end(); it++){
	mergehit.push_back(*it);
	it = hit2.erase(it);
	it--;
      }
}
*/





int view,id[Cview][Chit],pln[Cview][Chit],ch[Cview][Chit],hit[Cview],hitnum[Cview][Cpln][Cch],used[Cview][Chit];//B2

float pe[Cview][Chit];
int PLN,PLN2,CH,CL,CL2,CLHIT,CLHIT2,CLHIT3,CELL,CELL2,NEI,TRA,TRA2,TRACELL,TRACL,TRACL2,HIT,DIST,DIST2,DIST3,dummy,TMP;

bool hitcl[Cview][Cpln][Ccls];//B2

int clchi[Cview][Cpln][Ccls],clchf[Cview][Cpln][Ccls],ncl[Cview][Cpln],numcl[Cview][Cpln][Ccls],clhit[Cview][Cpln][Ccls][Cch];//B2
float clcenter[Cview][Cpln][Ccls],clpe[Cview][Cpln][Ccls];//B2
int clused[Cview][Cpln][Ccls];//B2

int cellu[Cview][Cpln][Ccell][Cdist],celld[Cview][Cpln][Ccell][Cdist],ncell[Cview][Cpln][Cdist],value[Cview][Cpln][Ccell][Cdist],nvalue[Cview][Cpln][Ccell][Cdist];//B2

bool neibor[Cview][Cpln][Ccell][Cdist][Cdist];//B2

int neiu[Cview][Cpln][Ccell][Cdist][Cdist],neid[Cview][Cpln][Ccell][Cdist][Cdist],nnei[Cview][Cpln][Cdist][Cdist];//B2

int track_cell[Cview][Ccell][Cpln],track_pln[Cview][Ccell][Cpln],track_dist[Cview][Ccell][Cpln],ntracell[Cview][Ccell],ntracl[Cview][Ccell],ntracl2[Cview][Ccell],ntrack[Cview],ntrack2[Cview],ntrack3[Cview],trank[Cview][Cpln][Cpln],ncltra[Cview][Cpln],rank[Cview][Ccell];//B2

bool ttrack[Cview][Ccell],ttrack2[Cview][Ccell];//B2

int plane[Cview][Ctra][Cpln],clus[Cview][Ctra][Cpln];//B2

bool vetowtracking[Cview][Ctra],edgewtracking[Cview][Ctra],track_stop[Cview][Ctra];//B2

float vetodist[Cview][Ctra];

bool ovcl[Cview][Cpln][Ccls];//add//B2

float dis;

float XX[Cview][Ctra][Cpln],YY[Cview][Ctra][Cpln],Xe[Cview][Ctra][Cpln],Yel[Cview][Ctra][Cpln],Yeh[Cview][Ctra][Cpln];//B2

float x[Cdir],y[Cdir],xe[Cdir],yel[Cdir],yeh[Cdir];
float chi2,tmp;

float par[Cview][Ctra][2];//2 is number of fitting parameter(p0,p1)

float trape[Cview][Ctra];//B2

float ulimit;
bool upst,dwst;
//int select;
int nhit;
int ue,shita;

int hit_n[Cview][Chit],hit_id[Cview][Chit][Chit];//B2

bool isohit[Cview][Ctra][Chit];//B2

//double neibor_th = 100.;
//double neibor_th = 10.;
//double neibor_th = 5.;
double neibor_th = 3.;

double joint_th = 25.;



bool fTracking(int mod, int axis=0, int N_long=1){
  memset(hit,0,sizeof(hit));
  memset(hitnum,-1,sizeof(hitnum));
  if(mod!=1&&mod!=2){//?????????????????????
    alltrack.clear();
  }
  for(int i=0; i<(int)hitcls.size(); i++){
    view=hitcls[i].view;
    id[view][hit[view]]=hitcls[i].id;
    pln[view][hit[view]]=hitcls[i].pln;
    ch[view][hit[view]]=hitcls[i].ch;
    pe[view][hit[view]]=hitcls[i].pe;
    used[view][hit[view]]=hitcls[i].used;
    //pdg[view][hit[view]]=hitcls[i].pdg;
    //cout<<mod<<" "<<view<<" "<<hit[view]<<" "<<pln[view][hit[view]]<<" "<<ch[view][hit[view]]<<endl;
    if(pln[view][hit[view]]<plnmax(mod,view,pln[view][hit[view]],axis))hitnum[view][pln[view][hit[view]]][ch[view][hit[view]]]=hit[view];
    hit[view]++;
  }


};


bool fTracking_loli(int mod){

  	alltrack.clear();
	int N_long=5;
	vector<Hit> hit_temp;
	//recon long track along z axis
	hit_temp=hitcls;
        B2_Dimension *fdim_temp = new B2_Dimension();
	for(int i=0; i<hitcls.size(); i++){
		int reconpln,reconch;
        	fdim_temp -> get_reconplnch_loli( mod, hitcls[i].view, hitcls[i].pln, hitcls[i].ch, 0, &reconpln, &reconch ); //mod view pln axis
		//cout<<mod<<" "<<hitcls[i].view<<" "<<hitcls[i].pln<<" "<<hitcls[i].ch<<" "<<reconpln<<" "<<reconch<<endl;
		hitcls[i].pln  = reconpln;
		hitcls[i].ch   = reconch;
		hitcls[i].used = 0;
	}
	fTracking(mod,0,N_long);

	//recon long track along xy axis
	for(int i=0;i<alltrack.size();i++){
		//for(int j=0;j<alltrack[i].hitid.size();j++){
		for(int j=1;j<alltrack[i].hitid.size();j++){
      			vector<Hit>::iterator it;
      			for(it = hit_temp.begin() ; it != hit_temp.end(); it++){
				if(it->id==alltrack[i].hitid[j]){
					//hit_temp.erase(it);
					//it--;
					it->used=1;
				}
			}
		}
	}
	hitcls = hit_temp;
	for(int i=0; i<hitcls.size(); i++){
		int reconpln,reconch;
        	fdim_temp -> get_reconplnch_loli( mod, hitcls[i].view, hitcls[i].pln, hitcls[i].ch, 1, &reconpln, &reconch ); //mod view pln axis
		hitcls[i].pln= reconpln;
		hitcls[i].ch = reconch;
	}
	fTracking(mod,1,N_long);





	//recon along z axis
	for(int i=0;i<alltrack.size();i++){
		//for(int j=0;j<alltrack[i].hitid.size();j++){
		for(int j=1;j<alltrack[i].hitid.size();j++){
      			vector<Hit>::iterator it;
      			for(it = hit_temp.begin() ; it != hit_temp.end(); it++){
				if(it->id==alltrack[i].hitid[j]){
					//hit_temp.erase(it);
					//it--;
					it->used=1;
				}
			}
		}
	}
	hitcls = hit_temp;
	for(int i=0; i<hitcls.size(); i++){
		int reconpln,reconch;
        	fdim_temp -> get_reconplnch_loli( mod, hitcls[i].view, hitcls[i].pln, hitcls[i].ch, 0, &reconpln, &reconch ); //mod view pln axis
		hitcls[i].pln  = reconpln;
		hitcls[i].ch   = reconch;
	}
	fTracking(mod,0,1);
	//recon track along xy axis
	for(int i=0;i<alltrack.size();i++){
		//for(int j=0;j<alltrack[i].hitid.size();j++){
		for(int j=1;j<alltrack[i].hitid.size();j++){
      			vector<Hit>::iterator it;
      			for(it = hit_temp.begin() ; it != hit_temp.end(); it++){
				if(it->id==alltrack[i].hitid[j]){
					//hit_temp.erase(it);
					//it--;
					it->used=1;
				}
			}
		}
	}
	hitcls = hit_temp;
	for(int i=0; i<hitcls.size(); i++){
		int reconpln,reconch;
        	fdim_temp -> get_reconplnch_loli( mod, hitcls[i].view, hitcls[i].pln, hitcls[i].ch, 1, &reconpln, &reconch ); //mod view pln axis
		hitcls[i].pln= reconpln;
		hitcls[i].ch = reconch;
	}
	fTracking(mod,1,1);





	delete fdim_temp;
	hit_temp.clear();

    return true;
};

bool fFindNearestTracks(int* a, int* b, float angle_th, float dist_th){
    /* Function to find two nearest tracks to be connected for water module.
     * Conditions:
     * (1) Angle between two tracks should be less than "angle_th".
     * (2) One track's position should be lower than the other track.
     * (3) Two tracks should be near close each other.
     *     (extending two tracks, the distance between two tracks at the middle point should be less than "dist_th".)
     * If more than one candidate is found, calculate joilik=sqrt((angle/angle_th)^2+(dist/dist_th)^2) for each candidate and select minimum one.
     * If no candidate is found, return false.
     */
    cout << "Find nearest tracks" << endl;
    const int Ntrack=(int)alltrack.size();
    float joilik=1e-6;
    bool candidate=false;
    for(int i=0; i<Ntrack; i++){
        for(int j=0; j<i; j++){
            if(alltrack[i].view!=alltrack[j].view) continue;
            float angle;
            if(fabs(alltrack[i].ang-alltrack[j].ang)<90.0) angle=fabs(alltrack[i].ang-alltrack[j].ang);
            else angle=180.0-fabs(alltrack[i].ang-alltrack[j].ang);
            if(angle>angle_th) continue; // condidtion (1)

            vector<vector<vector<float> > > pos(2, vector<vector<float> >(2, vector<float>(2, 0.0)));;
            pos[0][0][0]=alltrack[i].ixy;
            pos[0][0][1]=alltrack[i].iz;
            pos[0][1][0]=alltrack[i].fxy;
            pos[0][1][1]=alltrack[i].fz;
            pos[1][0][0]=alltrack[j].ixy;
            pos[1][0][1]=alltrack[j].iz;
            pos[1][1][0]=alltrack[j].fxy;
            pos[1][1][1]=alltrack[j].fz;

            /*  Let theta to be mean of two tracks,
             *  if -45 < theta < 45 then fitaxis=0 (fit along z-axis),
             *  else fitaxis=1 (fit along xy-axis).
             */
            int fitaxis=(fabs(alltrack[i].ang-alltrack[j].ang)>90.0 || fabs(alltrack[i].ang+alltrack[j].ang)>90.0);
            cout << "fitaxis " << fitaxis << " angle i " << alltrack[i].ang << " angle j " << alltrack[j].ang << endl;
            if(fitaxis){
                swap(pos[0][0][0], pos[0][0][1]);
                swap(pos[0][1][0], pos[0][1][1]);
                swap(pos[1][0][0], pos[1][0][1]);
                swap(pos[1][1][0], pos[1][1][1]);
            }
            //** swap positions in the order
            if(pos[0][0][1]>pos[0][1][1]) swap(pos[0][0], pos[0][1]);
            if(pos[1][0][1]>pos[1][1][1]) swap(pos[1][0], pos[1][1]);
            if(pos[0][0][1]>pos[1][0][1]) swap(pos[0]   , pos[1]   );
            if(pos[0][1][1]>pos[1][0][1]) continue; // condidion (2)
            //** dist is the distance between two tracks at the middle points.
            float dist=fabs(((pos[0][0][0]-pos[0][1][0])/(pos[0][0][1]-pos[0][1][1])+(pos[1][0][0]-pos[1][1][0])/(pos[1][0][1]-pos[1][1][1]))*(pos[0][1][1]-pos[1][0][1])/2.0-pos[0][1][0]+pos[1][0][0]);
            if(dist>dist_th) continue; // condition (3)

            if(!candidate || joilik>sqrt(angle*angle/angle_th/angle_th+dist*dist/dist_th/dist_th)){
                //** if more than one candidate is found, ...
                *a=i; *b=j;
                joilik=sqrt(angle*angle/angle_th/angle_th+dist*dist/dist_th/dist_th);
                candidate=true;
            }
        }
    }
    return candidate;
};

void fGetHitFromTrack(vector<Hit> &hit, const vector<Track> &track){
    cout << "Get hit from track" << endl;
    vector<Hit>::iterator it;
    for(int i=0; i<(int)track.size(); i++){
        for(int j=0; j<(int)track[i].hitid.size(); j++){
   	   for(it=hitcls_for_joint.begin(); it!=hitcls_for_joint.end(); it++){
                if(it->id==track[i].hitid[j]){
                    hit.push_back(*it);
                    break;
                }
            }
        }
    }
};

//void fFitAllHit(const vector<Hit> &hit, vector<float> &par, int axis, bool disp=false, int mod=15){
void fFitAllHit(const vector<Hit> &hit, vector<float> &par, int axis, bool disp=false, int mod=1){
    cout << "Fit all hit" << endl;
    vector<float> x, y, xe, ye;
    const int Nhits=(int)hit.size();
    B2_Dimension* fdim=new B2_Dimension();
    for(int i=0; i<Nhits; i++){
        int reconpln, reconch;
        fdim->get_reconplnch_loli(mod, hit[i].view, hit[i].pln, hit[i].ch, axis, &reconpln, &reconch);
        x.push_back (zposi   (mod, hit[i].view, reconpln, reconch, axis));
        y.push_back (xyposi  (mod, hit[i].view, reconpln, reconch, axis));
        xe.push_back(scithick(mod, hit[i].view, reconpln, y.at(i), axis)/sqrt(3));
        ye.push_back(sciwidth(mod, hit[i].view, reconpln, reconch, axis)/sqrt(3));
	std::cout << mod <<  " " << hit[i].view <<  " " << hit[i].pln <<  " " << hit[i].ch << " " << reconpln <<  " " << reconch <<  " " << axis << " " << x[i] << " " << y[i] << std::endl;
    }
    delete fdim;
    TGraphErrors* g=new TGraphErrors(Nhits, &x[0], &y[0], &xe[0], &ye[0]);
    TF1 *f=new TF1("f", "[0]+[1]*x");
    const float deltamin=0.1;
    const float deltax=(fabs(x[Nhits-1]-x[0])<deltamin ? deltamin : (x[Nhits-1]-x[0])); //avoid initial parameter=0
    const float deltay=(fabs(y[Nhits-1]-y[0])<deltamin ? deltamin : (y[Nhits-1]-y[0]));
    f->SetParameters(y[0]-x[0]*deltay/deltax, deltay/deltax);
    cout << "par before fit: " << f->GetParameter(0) << ", " << f->GetParameter(1) << endl;
    g->Fit("f", "WQ"); //ignore errors. Any better solutions?
    //cout << "par after  fit: " << f->GetParameter(0) << ", " << f->GetParameter(1) << " / Chisquare " << f->GetChisquare() << endl;
    cout << "par after  fit: " << f->GetParameter(0) << ", " << f->GetParameter(1) << " / Chisquare " << f->GetChisquare() << " " << 180./3.14*atan(f->GetParameter(1)) << " " << 180./3.14*atan(1./f->GetParameter(1)) << endl;
    if(disp){
        TCanvas* c_allhit=new TCanvas("c_allhit", "c_allhit");
        g->Draw("AP");
        c_allhit->Update();
        cout << "Enter to continue...";
        getchar();
        delete c_allhit;
    }
    par.push_back(f->GetParameter(0));
    par.push_back(f->GetParameter(1));
    f->Delete();
    g->Delete();
};

//Track fGetConnectedTrack(const vector<Hit> &hit, const vector<Track> &track, const vector<float> &par, int axis, int mod=15){
Track fGetConnectedTrack(const vector<Hit> &hit, const vector<Track> &track, const vector<float> &par, int axis, int mod=1){
    cout << "Get connected track" << endl;
    Track tr;
    tr.clear();
    tr.mod    = mod;
    tr.view   = hit[0].view;
    //tr.intcpt = par[0];
    //tr.slope  = par[1];
    //tr.ang    = atan(par[1])*180/3.141592;

    //tr.clstime //blank
    //tr.vetodist //not used for water module

    for(int i=0; i<(int)track.size(); i++){
        if(track[i].veto) tr.veto=true;
        if(track[i].edge) tr.edge=true;
        if(track[i].stop) tr.stop=true;
    }

    //** intcpt, slope, angle, pln, xy, z **//
    B2_Dimension* fdim=new B2_Dimension();
    if(axis==0){
        tr.intcpt = par[0];
        tr.slope  = par[1];
        tr.ang    = atan(tr.slope)*180.0/3.141592;

        //** Initialize **//
        int tmppln, tmpch;
        fdim->get_reconplnch_loli(mod, hit[0].view, hit[0].pln, hit[0].ch, 0, &tmppln, &tmpch);
	// ML use tmppln (0->23) for evaluating ipln,fpln
        tr.ipln = tmppln;
        tr.fpln = tmppln;
        tr.iz   = zposi(mod, hit[0].view, tmppln, tmpch);
        tr.fz   = zposi(mod, hit[0].view, tmppln, tmpch);
        tr.ixy  = par[0]+par[1]*tr.iz;
        tr.fxy  = par[0]+par[1]*tr.fz;

        for(int i=0; i<(int)hit.size(); i++){
            int reconpln, reconch;
            fdim->get_reconplnch_loli(mod, hit[i].view, hit[i].pln, hit[i].ch, 0, &reconpln, &reconch);
            if(tr.iz>zposi(mod, hit[i].view, reconpln, reconch)){
                tr.ipln = reconpln;
                tr.iz   = zposi(mod, hit[i].view, reconpln, reconch);
                tr.ixy  = par[0]+par[1]*tr.iz;
            }
            if(tr.fz<zposi(mod, hit[i].view, reconpln, reconch)){
                tr.fpln = reconpln;
                tr.fz   = zposi(mod, hit[i].view, reconpln, reconch);
                tr.fxy  = par[0]+par[1]*tr.fz;
            }
        }
    } //axis==0
    else{ //axis==1
        if(par[1]==0){
            tr.intcpt = -par[0]*1e+8;
            tr.slope  = 1e+8;
            tr.ang    = 90.0;
        }
        else{
            tr.intcpt = -par[0]/par[1];
            tr.slope  = 1.0/par[1];
            tr.ang    = atan(tr.slope)*180.0/3.141592;
        }

        //** Initialize **//
        int tmppln, tmpch;
        fdim->get_reconplnch_loli(mod, hit[0].view, hit[0].pln, hit[0].ch, 0, &tmppln, &tmpch);
        tr.ipln = tmppln;
        tr.fpln = tmppln;
        tr.ixy  = xyposi(mod, hit[0].view, tmppln, tmpch);
        tr.fxy  = xyposi(mod, hit[0].view, tmppln, tmpch);
        tr.iz   = par[0]+par[1]*tr.ixy;
        tr.fz   = par[0]+par[1]*tr.fxy;

        for(int i=0; i<(int)hit.size(); i++){
            int reconpln, reconch;
            fdim->get_reconplnch_loli(mod, hit[i].view, hit[i].pln, hit[i].ch, 0, &reconpln, &reconch);
            if(par[1]>0 != tr.ixy<xyposi(mod, hit[i].view, reconpln, reconch)){
                tr.ipln = reconpln;
                tr.ixy  = xyposi(mod, hit[i].view, reconpln, reconch);
                tr.iz   = par[0]+par[1]*tr.ixy;
            }
            if(par[1]>0 != tr.fxy>xyposi(mod, hit[i].view, reconpln, reconch)){
                tr.fpln = reconpln;
                tr.fxy  = xyposi(mod, hit[i].view, reconpln, reconch);
                tr.fz   = par[0]+par[1]*tr.fxy;
            }
        }
    } //axis==1
    delete fdim;

    //** hitid, isohit **//
    for(int i=0; i<(int)hit.size(); i++){
        tr.hitid.push_back(hit[i].id);
        bool isohit=true;
        for(int j=0; j<(int)alltrack.size(); j++){ //Here alltrack does not include tracks to be connected.
            for(int k=0; k<(int)alltrack[j].hitid.size(); k++){
                if(hit[i].id==alltrack[j].hitid[k]){
                    isohit=false;
                    break;
                }
            }
            if(!isohit) break;
        }
        tr.isohit.push_back(isohit);
    }

    return tr;
};

bool fConnectTracks(float angle_th, float dist_th){ //hosomi 160622
    /* Connect break two tracks due to gap between layers in water module.
     * Procedure:
     * - Find nearest tracks
     * - Get hit info from two tracks to be connected
     * - Fit all hits to be connected
     * - Get connected trak info
     * If no candidate is found, return false
     */
    int a, b;
    if(!fFindNearestTracks(&a, &b, angle_th, dist_th)) return false;
    for(int i=0; i<(int)alltrack[a].hitid.size(); i++) cout << "track a hitid: " << alltrack[a].hitid[i] << " isohit: " << alltrack[a].isohit[i] << endl;
    for(int i=0; i<(int)alltrack[b].hitid.size(); i++) cout << "track b hitid: " << alltrack[b].hitid[i] << " isohit: " << alltrack[b].isohit[i] <<endl;

    vector<Hit> hit_connect;
    vector<Track> track_connect;
    track_connect.push_back(alltrack[a]);
    track_connect.push_back(alltrack[b]);
    fGetHitFromTrack(hit_connect, track_connect);
    if(hit_connect.size()<2) return false;

    /*  Let theta to be mean of two tracks,
     *  if -45 < theta < 45 then fitaxis=0 (fit along z-axis),
     *  else fitaxis=1 (fit along xy-axis).
     */
    int fitaxis=(fabs(alltrack[a].ang-alltrack[b].ang)>90.0 || fabs(alltrack[a].ang+alltrack[b].ang)>90.0);
    cout << "fitaxis " << fitaxis << " angle a " << alltrack[a].ang << " angle b " << alltrack[b].ang << endl;
    vector<float> par;
    fFitAllHit(hit_connect, par, fitaxis);

    alltrack.erase(alltrack.begin()+a);
    alltrack.erase(alltrack.begin()+b);
    alltrack.push_back(fGetConnectedTrack(hit_connect, track_connect, par, fitaxis));

    return true;
};

void fGetTrackInfo(char* buf, int ievt, int Nconnect){
    int ntracks=(int)alltrack.size();
    int nview=0;
    for(int i=0; i<ntracks; i++) nview += alltrack[i].view;
    //char buf[64];
    sprintf(buf, "%d\t%d\t%d\t%d", ievt, ntracks, nview, Nconnect);
}



#endif
