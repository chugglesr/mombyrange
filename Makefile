CPP             = g++
CXXFLAGS        = -g -O3 -Wall -fPIC -D_REENTRANT -Wno-deprecated

ROOTCFLAGS      := $(shell root-config --cflags)
ROOTLIBS        := $(shell root-config --libs)
ROOTGLIBS       := $(shell root-config --glibs)
CXXFLAGS        += $(ROOTCFLAGS)


LIB = ---LocationOf---/lib_wagana

LIBS            = $(ROOTGLIBS) \
          -lm $(LIB)/B2EVENTSUMMARY.so $(LIB)/B2HitSummary.so \
          $(LIB)/B2SimHitSummary.so $(LIB)/B2SimVertexSummary.so \
          $(LIB)/B2SimParticleSummary.so $(LIB)/BeamInfoSummary.so \
          $(LIB)/B2BasicReconSummary.so $(LIB)/B2TrackSummary.so \
          $(LIB)/B2ReconSummary.so $(LIB)/B2AnaSummary.so \
          $(LIB)/NeutInfoSummary.so \

LFLAG	= ---LocationOf---/WAGASCI-BabyMIND-MC-master/lib

LFLAGS	=-L$(LFLAG)/B2SimVertexSummary.h \
	-L$(LFLAG)/B2EVENTSUMMARY.h \
	-L$(LFLAG)/B2SimHitSummary.h \
	-L$(LFLAG)/B2HitSummary.h \
	-L$(LFLAG)/B2SimParticleSummary.h \
	-L$(LFLAG)/BeamInfoSummary.h \
	-L$(LFLAG)/B2BasicReconSummary.h \
	-L$(LFLAG)/B2ReconSummary.h \
	-L$(LFLAG)/B2AnaSummary.h \
	-L$(LFLAG)/B2TrackSummary.h \
	-L$(LFLAG)/NeutInfoSummary.h


IFLAGS	=-I$(ROOTSYS)/include/

SRC = ./src
INC = ./inc

CXXFLAGS += -I$(LIB) -I$(SRC) -I$(INC)

TARGET = range

all: $(TARGET)

range: ./range.cpp
	$(CPP) $(CXXFLAGS) ./range.cpp -o $@ $(LIBS) $(LFLAGS) 

%.o: %.cpp 
	@echo "Start Compiling $<"
	@$(CPP) $(CXXFLAGS) -c $<
	@echo ".. Compiling Object Files $<   --> done"
	@echo ""

clean:  rm range
